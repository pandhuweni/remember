
//Declare
var $modalLogin = $('modalLogin');
var $failedAlert = $('failedAlert');
var $email = $('email');
var $password = $('password');
var $base_url = 'https://reminder-engine.herokuapp.com/';

//$('#failedAlert').hide();

 $('#failedAlert').hide();

$('#btnLogin').on('click',function(){
	var login = {
		email = $email.val();
		password = $password.val();
	};


	$.ajax({
		type:'POST',
		url : $base_url + '/users/sessions',
		data : login,
		success : function(){
			$modalLogin.hide();
		},
		error : function(){
			$failedAlert.show();
		}
	})
});