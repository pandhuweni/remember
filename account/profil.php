<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login to your Reminder</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/styles.css" rel="stylesheet">
    <link href="../css/styles.css" rel="stylesheet">
    <link href="../css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
	  <div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="#">Reminder</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
			<li><a href="#">Home <span class="sr-only">(current)</span></a></li>
			<li  class="active"><a href="profil.php">Profile</a></li>       
			<li><a href="#">Timeline</a></li>        
		  </ul>
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="#">Bantuan</a></li>
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Account <span class="caret"></span></a>
			  <ul class="dropdown-menu">
				<li><a href="#">Setting</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="#">Logout</a></li>
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
		<div class="container" style="margin-top:50px">
			<div class="row">
				<div class="col-md-2">
					<img src="../img/person-flat.png" alt="..." class="img-responsive img-circle">
				</div>
				<div class="col-md-10">					
					<h2>Nila Nurlitasari</h2>
						<div id="textProfile">
							<h4><span class="glyphicon glyphicon-envelope"></span> &nbsp;nilanurlitasari@gmail.coom</h4>
							<h4><span class="glyphicon glyphicon-user"></span> &nbsp;Perempuan</h4>
							<h4><span class="glyphicon glyphicon-calendar"></span> &nbsp;19 Maret 1997</h4>	
						</div>
						<div id="editProfile" >
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-envelope"></span> &nbsp;</span>
							  <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1" value="nilanurlitasari@gmail">
							</div>
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span> &nbsp;</span>
							  <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1" value="Perempuan">
							</div>
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span> &nbsp;</span>
							  <input type="date" class="form-control" placeholder="Username" aria-describedby="basic-addon1" value="03/19/1997">
							</div>
						</div>
					
					<button class="btn btn-default" id="edit" type="submit">Edit</button>
					<button class="btn btn-primary" id="save" type="submit">Save</button>
					
				</div>
			</div>
		</div>  
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery-1.12.3.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
	<script src="../js/jquery.dataTables.min.js"></script>
	<script src="../js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tb_reminder').DataTable();
			$('#editProfile').hide()
		} );
	</script>
	<script>
		var textProfile = $( "#textProfile" );		
		var editProfile = $( "#editProfile" );
		$( "#save" ).on( "click", function( event ) {
		  textProfile.show();
		  editProfile.hide();
		});
		$( "#edit" ).on( "click", function( event ) {
		  textProfile.hide();
		  editProfile.show();
		});
	</script>
  </body>
</html>